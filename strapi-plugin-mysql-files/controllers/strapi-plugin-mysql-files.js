const Path = require('path');
const URL = require('url');

const modulesDir = Path.join(process.cwd(), 'node_modules');
let modulesPath = '';

if (__dirname.indexOf(modulesDir) == -1)
  modulesPath = `${modulesDir}/`;

const mysql = require(modulesPath + 'mysql');

strapi.app.use(async function (ctx, next) {

  const config = strapi.plugins.upload.config;
  const uploadTable = config.providerOptions.table || 'provider-upload-mysql';

  const {method, url} = ctx.req;

  //If we're not querying this plugin ==> next
  if (method != 'GET' || url.indexOf('/' + uploadTable + '/') != 0)
    return await next();

  const parsed = URL.parse(url);
  const fileHash = decodeURIComponent(Path.basename(parsed.pathname));

  const connection = mysql.createConnection({
    host     : config.providerOptions.host,
    user     : config.providerOptions.user,
    password : config.providerOptions.password,
    database : config.providerOptions.database
  });

  await new Promise((resolve, reject) => {
    connection.query(`SELECT name, mime, data FROM \`${uploadTable}\` WHERE \`hash\`="${fileHash}"`, function(err, rows, fields) {
      if (err) reject(err);
      resolve(rows[0])
    });
  }).then((file) => {
    ctx.append('Content-Type', file.mime || 'application/octet-stream');
    ctx.body = file.data;
  }).catch((err) => {
    strapi.log.error(`Couldn't download file ${file.name}`);
    return strapi.log.error(err);
  })

})
