# strapi-provider-mysql-files

Strapi provider for file uploading to Mysql as blobs
Based on [strapi-mongodb-files](https://github.com/AHgPuK/strapi-mongodb-files)

#Installation process see at: 

[strapi-mysql-files repo](https://gitlab.com/themineraria/strapi_mysql_files#readme)

### License

- [MIT License](LICENSE.md)

### Links

- [Strapi website](http://strapi.io/)
- [Strapi community on Slack](http://slack.strapi.io)
- [Strapi news on Twitter](https://twitter.com/strapijs)
