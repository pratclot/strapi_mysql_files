'use strict';

// Module dependencies
const Path = require('path');
const modulesDir = Path.join(process.cwd(), 'node_modules');
let modulesPath = '';

if (__dirname.indexOf(modulesDir) == -1)
	modulesPath = `${modulesDir}/`;

const mysql = require(modulesPath + 'mysql');

/* eslint-disable no-unused-vars */
module.exports = {
	provider: 'Mysql',
	name: 'Mysql files',

	init: (config) => {

		const connection = mysql.createConnection({
			host     : config.host,
			user     : config.user,
			password : config.password,
			database : config.database
		});

		const uploadTable = config.table || 'provider-upload-mysql';

		connection.connect(function(err) {
			if (err) {
				const TAG = 'Mysql files provider :';
				strapi.log.error(TAG, `Couldn't access to database`);
				strapi.log.error(err);
				return;
			}
		});

		let createTable = `create table if not exists \`${uploadTable}\`(
			id int primary key auto_increment,
			name varchar(255) not null,
			hash varchar(255) not null,
			mime varchar(255) not null,
			data longblob not null
		)`;

		connection.query(createTable, function(err, results, fields) {
			if (err) {
				const TAG = 'Mysql files provider :';
				strapi.log.error(TAG, `Couldn't create or access to table`);
				strapi.log.error(err)
				return;
			}
		});

		return {
			upload: (file) => {
				const TAG = 'Mysql files provider upload:';
				return new Promise(function(resolve, reject) {
					connection.query(`SELECT COUNT(*) AS count FROM \`${uploadTable}\` WHERE \`hash\`="${file.hash}"`, function(err, rows, fields) {
						if (err) return reject(err);

						if(rows[0].count === 0)
						{
							connection.query(`INSERT INTO \`${uploadTable}\` (name, hash, mime, data) VALUES (?, ?, ?, BINARY(?))`, [file.name, file.hash, file.mime, file.buffer], function(err, res) {
								if (err) return reject(err);
								else {
									file.url = `/${uploadTable}/${file.hash}`
									resolve();
								}
							});
						}
						else return reject(new Error(`The file ${file.name} already exists`));
					});
				});
			},

			delete: async (file) => {
				const TAG = 'Mysql files provider delete:';
				return new Promise(function(resolve, reject) {
					connection.query(`DELETE FROM \`${uploadTable}\` WHERE \`hash\`="${file.hash}"`, function(err, res, fields) {
						if (err) reject(err);
						else {
							if (res.affectedRows === 0) {
								return reject(new Error(`File ${file.name} not found`));
							} else {
								resolve();
							}
						}
					});
				});
			}
		};
	}
};
